# Lua Virtual Machine

These are the design documents for my Lua Virtual Machine.

## Memory segments

First the memory. At any moment, the machine has several small segments of
memory accessible:

  * the "contextual memory" section used to store local variables, function arguments, and upvalues,
  * the general stack, where most of the stuff happens.
  * The "static" memory where are stored stuff like function templates and string litterals

### Value recycling

There is a decent possibility that any of the non-stack
values are modified outside of the control of the "current" function, so one
should not (at first) re-use a value once it's pushed onto the stack.

For instance, in the same code branch, we first define a function `f` capturing
the variable `a`, and mutating it. We then have a series of operations using `a`
within the code branch, but also with calls to `f`. If we push `a` at the beginning
of the series of operations, and clone the value when needed, the value will be stale
after any call to `f`, even though no instruction within the code branch has mutated
the value directly.

## Function data

A function must have the following metadata :

* upvalue references, with an upvalue a "simple" `Rc<Cell<LuaValue>>`
* expected amount of arguments (`usize`)
* max size of contextual memory
* max stack size (int), should be statically computable.
* an array of code "branches", the first branch being the main branch (the root)

### Code branch

A code branch is a vector of opcodes, with stable stack activity on them, and
two integers representing the range of local variables it's going to use.

By stable activity it is meant that the stack should have the same content
before and after the execution of the code branch, but it can grow during the
execution itself.

## Function call

When there's a function call with a given amount of arguments, there are
several things to do.

* Allocate an execution stack space;
* Allocate an array of `Rc<Cell<LuaValue>>` for the locals, initialized to
  `nil`. That would be the contextual memory segment for this callstack frame;
* Initialize the first local slots with the value of the arguments, up to the
  expected number of arguments defined by the function;
* Execute the main branch

## Code branch execution

The first thing to do is to reallocate the local variable slots for this
block so that we don't pollute any upvalue captured from the previous
execution of this block, which means new `Rc::new()` calls in the local range.

To be clear, the problem is in the way upvalues are captured by functions. A
capture is simply a copy of an `Rc<Cell>`, not a copy of the value itself.
Thus simply clearing the content of the cell would clear the content of any
previous upvalue.

Then, we take the first instruction, and off we go :)

## Function template

A function template is a function that hasn't had its upvalues defined.
Instead of direct reference, it's an int refering to the calling function's
local slot. Only at instanciation time is that int dereferenced into a
`Rc<Cell<LuaValue>>>`.

## Upvalue

A consequence of the function template thingy is that anytime a function with
an upvalue is defined, the outer function must have the same upvalue, even
though it won't use it directly.

## Error handling

In its first iteration, the plan is to do sanity checks on the instructions
operands before executing them, and use the usual Rust Result type for error
propagation up the call stack.

However, in the future it might be a good idea to have unchecked versions for
when we *know* it's going to be correct, by proving interesting properties in
the compiler directly. Of course, that would preclude the execution of bytecode
without knowing where it comes from, but IMO that's a small price to pay.