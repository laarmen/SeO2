# Virtual machine opcodes

## Litteral value push operations

### Instruction `PushFloat`

Pushes a float onto the stack.

#### Operand

The float value which should be pushed. `f32` vs `f64` TBD.

### Instruction `PushInt`

Pushes an integer onto the stack.

#### Operand

The integer value which should be pushed. Int size TBD.

### Instruction `PushString`

Pushes a static string onto the stack.

#### Operand

A reference to a string in the static memory segment (an int).

### Instruction `PushNil`

Pushes a nil value onto the stack

#### Operand

None.

### Instruction `PushTable`

Pushes a new, empty table.

#### Operand

None.

## Contextual memory interaction

### Instruction `PushContext`

Pushes the content of a context memory slot to the stack.

#### Operand

A reference to a contextual memory slot, which means a positive integer.

### Instruction `SetContext`

Pops the topmost value of the stack and stores it in the indicated contextual memory slot.

#### Operand

A reference to a contextual memory slot, which means a positive integer.

## Stack manipulation

Various instructions related to direct manipulation of the stack items

### `ResizeStack`

Ensures the stack has exactly the right number of items on it, discarding
elements or adding `nil` if need be.

#### Operand

Unsigned integer, should not exceed the max size of the stack for the function

### `CopyItem`

Make a copy of an item at an arbitrary index of the stack, and push it on
top of it.

#### Operand

Unsigned integer, indexing the value to copy.
